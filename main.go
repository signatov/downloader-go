package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"sync/atomic"
	"syscall"

	"github.com/pkg/errors"
)

const (
	maxQueuedThreads = 4
	maxShownResult   = 10
	bindAddress      = ":8080"
)

// TaskURL is a url to download.
type TaskURL struct {
	URL string
}

// TaskID is a identificator of created task.
type TaskID struct {
	ID uint64
}

type statusType int

const (
	new statusType = iota
	pending
	completed
	downloadError
)

var statusToJSON = map[statusType]string{
	new:           "NEW",
	pending:       "PENDING",
	completed:     "COMPLETED",
	downloadError: "ERROR",
}

func (s statusType) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(statusToJSON[s])
	buffer.WriteString(`"`)
	return buffer.Bytes(), nil
}

// Task is all information about task.
type Task struct {
	ID                    uint64
	URL                   string
	Status                statusType
	ResponseContentLength int64  `json:"Response content length"`
	ResponseHTTPStatus    int    `json:"Response HTTP status"`
	ResponseBody          string `json:"Response body"`

	mutex *sync.RWMutex
}

func newTask(id uint64, url string) *Task {
	task := &Task{
		ID:    id,
		URL:   url,
		mutex: &sync.RWMutex{},
	}
	return task
}

func (task *Task) clone() *Task {
	var newCopy *Task
	task.mutex.RLock()
	newCopy = &Task{
		ID:                    task.ID,
		URL:                   task.URL,
		Status:                task.Status,
		ResponseContentLength: task.ResponseContentLength,
		ResponseHTTPStatus:    task.ResponseHTTPStatus,
		ResponseBody:          task.ResponseBody,
	}
	task.mutex.RUnlock()
	return newCopy
}

// DownloaderService is service that starts download workers.
type DownloaderService struct {
	queue    chan (*Task)
	allTasks []*Task
	mutex    *sync.RWMutex
	wg       *sync.WaitGroup
}

func download(task *Task) {
	task.mutex.RLock()
	url := task.URL
	task.mutex.RUnlock()

	resp, err := http.Get(url)
	if err != nil {
		task.mutex.Lock()
		task.Status = downloadError
		task.mutex.Unlock()
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		task.mutex.Lock()
		task.Status = downloadError
		task.mutex.Unlock()
		return
	}

	task.mutex.Lock()
	task.ResponseBody = string(body[:len(body)])
	task.ResponseHTTPStatus = resp.StatusCode
	task.ResponseContentLength = resp.ContentLength
	task.Status = completed
	task.mutex.Unlock()
}

// NewDownloaderService is a function that makes new downloader service instance.
func NewDownloaderService(maxThreads int) *DownloaderService {
	srv := &DownloaderService{
		queue:    make(chan (*Task)),
		allTasks: make([]*Task, 0),
		mutex:    &sync.RWMutex{},
		wg:       &sync.WaitGroup{},
	}
	srv.wg.Add(maxQueuedThreads)
	for i := 0; i < maxQueuedThreads; i++ {
		go func(srv *DownloaderService) {
			defer srv.wg.Done()
			for task := range srv.queue {
				download(task)
			}
		}(srv)
	}
	return srv
}

func (srv *DownloaderService) appendTask(t *Task) {
	srv.queue <- t
	srv.mutex.Lock()
	srv.allTasks = append(srv.allTasks, t)
	srv.mutex.Unlock()
}

func (srv *DownloaderService) last(n int) []*Task {
	srv.mutex.RLock()
	m := len(srv.allTasks)
	if n > m {
		n = m
	}
	res := make([]*Task, 0)
	for i := m - n; i < m; i++ {
		res = append(res, srv.allTasks[i].clone())
	}
	srv.mutex.RUnlock()
	return res
}

func (srv *DownloaderService) shutdown() {
	close(srv.queue)
	srv.wg.Wait()
}

type appContext struct {
	srv    *DownloaderService
	nextID uint64
}

func (ctx *appContext) handleURL(w http.ResponseWriter, r *http.Request) (int, error) {
	decoder := json.NewDecoder(r.Body)
	var jsonBody TaskURL
	err := decoder.Decode(&jsonBody)
	if err != nil {
		return 404, errors.Wrap(err, "Resource not found")
	}

	id := atomic.AddUint64(&ctx.nextID, 1)
	ctx.srv.appendTask(newTask(id, jsonBody.URL))

	res := TaskID{ID: id}
	buf, err := json.Marshal(res)
	if err == nil {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write(buf)
	} else {
		http.Error(w, "Can't marshal structure to JSON message", http.StatusInternalServerError)
		return 500, errors.Wrap(err, "Can't marshal structure to JSON message")
	}
	return 200, nil
}

func (ctx *appContext) handleResultList(w http.ResponseWriter, r *http.Request) (int, error) {
	res := ctx.srv.last(maxShownResult)
	buf, err := json.Marshal(res)
	if err == nil {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write(buf)
	} else {
		http.Error(w, "Can't marshal structure to JSON message", http.StatusInternalServerError)
		return 500, errors.Wrap(err, "Can't marshal structure to JSON message")
	}
	return 200, nil
}

func (ctx *appContext) handleResult(w http.ResponseWriter, r *http.Request) (int, error) {
	key := r.URL.Query().Get("ID")
	if key == "" {
		return ctx.handleResultList(w, r)
	}
	id, err := strconv.Atoi(key)
	if err != nil {
		return 404, errors.Wrap(err, "Resource not found")
	}

	var task *Task
	found := false
	ctx.srv.mutex.RLock()
	if id <= 0 || id > len(ctx.srv.allTasks) {
		return 404, errors.Wrap(err, "Resource not found")
	}
	t := ctx.srv.allTasks[id-1]
	if t != nil {
		task = t.clone()
		found = true
	}
	ctx.srv.mutex.RUnlock()

	if !found {
		return 404, errors.Wrap(err, "Resource not found")
	}

	buf, err := json.Marshal(task)
	if err == nil {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write(buf)
	} else {
		http.Error(w, "Can't marshal structure to JSON message", http.StatusInternalServerError)
		return 500, errors.Wrap(err, "Can't marshal structure to JSON message")
	}
	return 200, nil
}

type safeHandler func(w http.ResponseWriter, r *http.Request) (int, error)

// ServerHttp is a HTTP-request processing handler.
func (fn safeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	status, err := fn(w, r)
	if err != nil {
		log.Printf("HTTP %d: %q", status, err)
		switch status {
		case http.StatusNotFound:
			http.NotFound(w, r)
		case http.StatusInternalServerError:
			http.Error(w, http.StatusText(status), status)
		default:
			http.Error(w, http.StatusText(status), status)
		}
	}
}

func main() {
	ctx := &appContext{srv: NewDownloaderService(maxQueuedThreads)}

	mux := http.NewServeMux()
	mux.Handle("/send", safeHandler(ctx.handleURL))
	mux.Handle("/result", safeHandler(ctx.handleResult))

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	server := &http.Server{Addr: bindAddress, Handler: mux}
	go func() {
		log.Fatal(server.ListenAndServe())
	}()
	log.Printf("The service is ready to listen %s and serve.", bindAddress)

	sig := <-interrupt
	switch sig {
	case os.Interrupt:
		log.Print("SIGINT...")
	case syscall.SIGTERM:
		log.Print("SIGTERM...")
	}

	ctx.srv.shutdown()
	server.Shutdown(context.Background())
}
